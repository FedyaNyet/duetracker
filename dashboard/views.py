from django.http import HttpResponse, HttpResponseRedirect
from django.core import serializers
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.views.generic import *
from django.views.generic.edit import FormMixin
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME, login, logout, authenticate
from django.db.models import Sum
from django.utils.decorators import method_decorator
from django.conf import settings
from braces.views import UserPassesTestMixin
from django.template import RequestContext
from django.template.loader import get_template

from dashboard.mixins import ActivePage, ActiveLoginRequiredMixin
from dashboard.forms import DueItemForm, LoginForm, UserCreateForm
from dashboard.models import DueItem, UserProfile

import json
from urllib.parse import urlparse


class HomeView(ActivePage, TemplateView):
    active = 'landing'
    template_name = 'landing.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView,self).get_context_data(**kwargs)
        context['login_form'] = LoginForm()
        return context


class LoginView(HomeView, FormView):
    form_class = LoginForm

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(LoginView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        return HttpResponseRedirect(reverse('dashboard'))

    def form_invalid(self, form):
        context = super(LoginView, self).get_context_data()
        context['login_form'] = form
        return self.render_to_response(context)

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('landing'))


class LogoutView(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self):
        logout(self.request)
        return reverse('landing')


class RegisterView(FormView):
    form_class = UserCreateForm
    template_name = 'forms/register.html'

    def get_context_data(self, **kwargs):
        context = super(RegisterView,self).get_context_data(**kwargs)
        context['register_form'] = self.form_class()
        context['login_form'] = LoginForm()
        return context

    def form_valid(self, form):
        user = form.save()
        profile = UserProfile(user=user)
        profile.save()
        # concider sending this with a queue
        confim_url = "http://"+settings.HTTP_HOST+reverse('confirm',kwargs={'hash':profile.hash})
        email_context = {'confirmation_url':confim_url, 'user':profile.user}
        html_message = get_template('emails/registration.html').render(email_context)
        send_mail('Complete DueTracker Registration', "", 'no-reply@duetracker.com', [user.email], fail_silently=False, html_message=html_message)
        messages.add_message(self.request, messages.SUCCESS, 'We sent you and email. Please confirm your address.')
        return HttpResponseRedirect(reverse('landing'))

    def form_invalid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        context['register_form'] = form
        return self.render_to_response(context)


class ConfirmView(RedirectView):

    def get(self, request, *args, **kwargs):
        if 'hash' not in kwargs:
            messages.add_message(self.request, messages.ERROR, 'Quit trying to break things.')
            return HttpResponseRedirect(reverse('landing'))
        profile = UserProfile.objects.filter(hash=kwargs['hash']).get()
        if profile:
            profile.user.is_active = True;
            profile.user.save()
            messages.add_message(self.request, messages.SUCCESS, 'Account Activated.')
        else:
            messages.add_message(self.request, messages.ERROR, 'Unable to confirm your Account with the provided code.')
        return HttpResponseRedirect(reverse('landing'))


class DashboardView(ActiveLoginRequiredMixin, ActivePage, TemplateView):
    active = 'dashboard'
    template_name = 'dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['data'] = serializers.serialize('json', DueItem.objects.filter(owner=self.request.user).all())
        context['form'] = DueItemForm()
        return context


class DueItemCreateView(ActiveLoginRequiredMixin, FormView):
    form_class = DueItemForm
    success_url = '/'
    template_name = 'forms/due_item.html';

    def get_form_kwargs(self):
        kwargs = {'initial': self.get_initial()}
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user;
        self.object.save()
        model_json = serializers.serialize('json', DueItem.objects.filter(owner=self.request.user).all())
        context = dict(
            items=json.loads(model_json),
            form=get_template('forms/due_item.html').render({'form': DueItemForm()})
        )
        return HttpResponse( json.dumps(context) , content_type="application/json")

    def form_invalid(self, form):
        rendered = get_template('forms/due_item.html').render({'form': form})
        return HttpResponse( json.dumps({'error': True, 'form': rendered}), content_type="application/json" )


class DueItemUpdateView(ActiveLoginRequiredMixin, UserPassesTestMixin, UpdateView):

    form_class = DueItemForm
    success_url = '/'
    model = DueItem
    template_name = 'forms/due_item.html';
    
    def test_func(self, user):
        return bool(DueItem.objects.filter(owner=self.request.user).get(pk=self.kwargs['pk']))

    def form_valid(self, form):
        form.save()
        data = json.loads(serializers.serialize('json', DueItem.objects.filter(owner=self.request.user).all()))
        return HttpResponse( json.dumps({'items':data}), content_type="application/json")

    def form_invalid(self, form):
        rendered = get_template('forms/due_item.html').render({'form': form})
        return HttpResponse( json.dumps({'error': True, 'form': rendered}), content_type="application/json" )


class DueItemDeleteView(ActiveLoginRequiredMixin, UserPassesTestMixin, View):

    def get_object(self):
        return DueItem.objects.filter(owner=self.request.user).get(pk=self.kwargs['pk'])
    
    def test_func(self, user):
        return bool(self.get_object())

    def get(self, request, *args, **kwargs):
        item = self.get_object();
        item.delete()
        data = serializers.serialize('json', DueItem.objects.filter(owner=self.request.user).all())
        return HttpResponse( data, content_type="application/json")
from django.conf.urls import url
from dashboard import views

urlpatterns = [
    url(r'^$', 	    	 						views.HomeView.as_view(), 					name='landing'),
    url(r'^dashboard/$', 	    	 			views.DashboardView.as_view(), 				name='dashboard'),
    url(r'^login/$',							views.LoginView.as_view(success_url='/'), 	name="login"),
    url(r'^logout/$', 							views.LogoutView.as_view(), 				name='logout'),
    url(r'^logout/$', 							views.LogoutView.as_view(), 				name='logout'),
    url(r'^register/$',  						views.RegisterView.as_view(), 				name='register'),
    url(r'^confirm/(?P<hash>[0-9a-z]+)/$',  	views.ConfirmView.as_view(), 				name='confirm'),
    url(r'^due_item/$',             			views.DueItemCreateView.as_view(),       	name='create_item'),
    url(r'^due_item/update/(?P<pk>[0-9]+)/$',  	views.DueItemUpdateView.as_view(), 			name='update_item'),
    url(r'^due_item/delete/(?P<pk>[0-9]+)/$',  	views.DueItemDeleteView.as_view(), 			name='delete_item')
]
from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm, DateInput, Form
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from dashboard.models import DueItem
from django.core.mail import send_mail


class DateInput(DateInput):
    input_type = 'date'


class BootstrapForm(Form):

    def __init__(self, *args, **kwargs):
        super(BootstrapForm, self).__init__(*args, **kwargs)
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = 'form-control'



class DueItemForm(ModelForm, BootstrapForm):

    def __init__(self, *args, **kwargs):
        super(DueItemForm, self).__init__(*args, **kwargs)
        self.fields['recurrence'].widget.attrs['ng-change'] = "due_date_recurrence_change(recurrence,'{?recurrence?}')";
        self.fields['recurrence'].widget.attrs['ng-model'] = 'recurrence';

    class Meta:
        model = DueItem
        fields = ['name', 'date', 'amount', 'recurrence']

class LoginForm(AuthenticationForm, BootstrapForm):

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'username'
        self.fields['password'].widget.attrs['placeholder'] = 'password'

        self.fields['username'].error_messages = {'required': 'Both Fields are required'}
        self.fields['password'].error_messages = {'required': 'Both Fields are required'}


class UserCreateForm(UserCreationForm, BootstrapForm):

    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.is_active = False
            user.save()
        return user
from django import template

register = template.Library()

@register.filter
def login_error(form): # Only one argument.
	for field in form.errors.as_data():
		if field is '__all__':
			if 'inactive' in form.errors[field][0]:
				return "Please activate your account."
			return "Incorrect username & password combination."
		return form.errors[field][0]
from django.views.generic.base import ContextMixin
from braces.views import LoginRequiredMixin, UserPassesTestMixin


class ActivePage(ContextMixin):
	active = "home"

	def get_context_data(self, **kwargs):
		context = super(ActivePage, self).get_context_data(**kwargs)
		context['active_page'] = self.active
		return context


class ActiveLoginRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):
    def test_func(self, user):
        return user.is_active
var myApp = angular.module('tracker', []);

myApp.config(function($interpolateProvider) {
	$interpolateProvider.startSymbol('{?');
	$interpolateProvider.endSymbol('?}');
});

myApp.filter('sum', function() {
	return function(data, field) {
		var sum = 0; 
		for (var i in data) {
			sum += parseFloat(data[i].fields[field]);
		}
		return sum;
	}
});

myApp.filter('date_range_total', function(){
	return function(data, date_range){
		if(!date_range)return 0.00;
		if(!Array.isArray(data)) data = [data];
    	$total = 0.00;
    	get_data_for_dates(data, date_range).forEach(function(obj, idx, arr){
    		// console.log(data, obj, date_range);
    		$total += parseFloat(obj.fields.amount) * obj.fields.dates.length;
    	});
    	return $total;
	}
})

myApp.filter('date_range_data', function() {
	return function(data, date_range) {
		return get_data_for_dates(data, date_range);
	}
});

myApp.controller('dashCtrl', ['$scope', '$http', '$compile', function($scope, $http, $compile) {

	$scope.data = data;
	$scope.ready = true;
	
	$http.defaults.headers.common['X-CSRFToken'] = csrf_token;
	$scope.date_range;

    $scope.due_date_recurrence_change = function(new_val, old_val, datepicker='.modal:visible .input-group.date'){
    	var options = {
		    todayHighlight: true
		}
    	if(new_val === 'CU'){
	    	options = {
			    clearBtn: true,
			    multidate: true,
			    todayHighlight: true
			}
    	}
	    $(datepicker).datepicker('remove').datepicker(options);
	    if( new_val === 'CU' || old_val === 'CU' ){
	    	$('.modal:visible .input-group.date input').val('');
	    }
    }

	$scope.add_item = function(pk){
		url = url_create_item;
		if(pk)
			url = url_update_item.replace(0,pk);
		$http
			.post(
				url, 
				item_form(), 
				{headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
			)
			.then(function(res){
				if(res.status!= 200) return;
				if(!res.data.error){
					$scope.data = res.data.items;
					dismiss_active_modal();
				}
				newForm = $('#new_due_item .modal-content').html(res.data.form);
				$scope.recurrence = null;
				$compile(newForm.contents())($scope);
				$scope.due_date_recurrence_change('', '', '#new_due_item .input-group.date');
			});
	};

	$scope.edit_item = function(item){
		update_url = url_update_item.replace('0',item.pk);
		$http
			.get(
				update_url
			)
			.then(function(res){
				if(res.status!= 200) return;
				newForm = $('#edit_due_item .modal-content').html(res.data);
				$compile(newForm.contents())($scope);
				new_val = '';
				selected = $('#edit_due_item  .modal-content').find('option[selected]')
				if(selected){
					new_val = selected.attr('value');
				}
				$('#edit_due_item  .modal-content').find('select').val(new_val);
				$scope.recurrence = new_val;
				$('#edit_due_item').modal('show');
				$scope.due_date_recurrence_change(new_val, null, '#edit_due_item .input-group.date');
			});
	}

	$scope.showDateRangePicker = function(){
		$('#reportrange').focus();
	};

	$scope.dates_display = function(item){
		if(!item.fields.dates)return"";
		if(item.fields.dates.length < 1){
			return "";
		}
		if(item.fields.dates.length > 1){
			return item.fields.dates.length +' '+ 'occurences';
		}
		return item.fields.dates[0].format('MMMM Do');
	}

	$scope.item_total = function(item){
		if(!item.fields.dates)return 0;
		return item.fields.dates.length * parseFloat(item.fields.amount);
	}

    $scope.frequency = function(short_hand){
    	switch(short_hand){
    		case 'MO':
    			return 'Monthly';
    		case 'YE':
    			return "Yearly";
    		case 'DA':
    			return 'Daily';
    		default:
    			return 'Custom';
    	}
    };

    $scope.delete = function(pk){
		delete_url = url_delete_item.replace('0',pk);
		$http
			.get(
				delete_url
			)
			.then(function(res){
				if(res.status!= 200) return;
				$scope.data = res.data
				dismiss_active_modal();
			});
    }

}]);

function get_dates_in_range(date_range, target_dates, frequency){
		
	frequencies = {'MO':'months','YE':'years', 'DA':'days','CU':'custom'}
	increment = frequencies[frequency];

	var start_date = date_range[0];
	var end_date = date_range[1];
	
	var dates = [];
	switch(frequency){
		case 'DA':
		case 'MO':
		case 'YE':
			var due_date = target_dates[0];
			for (var m = start_date; m.isBefore(end_date) || m.isSame(end_date); m.add(1,increment)) {
				var new_date = moment(m);
				if(frequency === 'MO')
					new_date.date(due_date.date());
				if(frequency === 'YE')
					new_date.date(due_date.date()).month(due_date.month());
				if(new_date.isBetween(start_date, end_date) || new_date.isSame(start_date) || new_date.isSame(end_date)){
					dates.push(new_date);
				}
			}
			break;
		case 'CU':
		default:
			for(var f = 0; f<target_dates.length; f++){
				due_date = target_dates[f];
				if(due_date.isBetween(start_date, end_date) || due_date.isSame(start_date) || due_date.isSame(end_date)){
					dates.push(due_date);
				}
			}
			break;
	}
	return dates;
}

function get_data_for_dates(data, date_range){
	if(!date_range)return data;

	var range = date_range.split(" - ");

	data.forEach(function(item, idx, arr){
		if(item.model !== "dashboard.dueitem") return;

		due_dates = item.fields.date.split(',');
		for(idx = 0; idx<due_dates.length; idx++){
			due_dates[idx] = moment(due_dates[idx], 'MM/DD/YYYY')
		}
		var start_date = moment(range[0], 'MMMM D, YYYY');
		var end_date = moment(range[1], 'MMMM D, YYYY');
		item.fields.dates = get_dates_in_range([start_date, end_date], due_dates, item.fields.recurrence);
	});

	return data.filter(function(item, idx, arr){
		if(item.model !== "dashboard.dueitem") return false;
		return data[idx].fields.dates.length > 0;
	});
}

function dismiss_active_modal(){
	$('.modal.in').modal('hide');
}

function item_form(){
	ret = {};
	$('.modal:visible form').serializeArray().forEach(function(currentValue, index, array){
		ret[currentValue.name] = currentValue.value;
	});
	return $.param(ret);
}


$(function() {

	$('body').on('submit', 'form', function(e){
		e.preventDefault();
	});

   	$('#reportrange').daterangepicker({
    	locale: {
	      format: 'MMMM D, YYYY'
	    },
	    startDate: moment().startOf('month'),
	    endDate: moment().endOf('month'),
        ranges: {
           'Today': [moment(), moment()],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Next Month': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')]
        }
    });

	$('#new_item_form .input-group.date').datepicker({ todayHighlight: true })

});


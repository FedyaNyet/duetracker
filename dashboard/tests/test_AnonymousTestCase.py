from dashboard.models import *
from dashboard.views import *
from django.test import TestCase #, RequestFactory
from django.test.client import Client
from django.contrib import auth
from django.contrib.auth.models import AnonymousUser, User
from django.core.urlresolvers import resolve
import requests

class AnoymousTestCase(TestCase):

    def setUp(self):
        self.client = Client()

        self.owner = User.objects.create_user(username='test-usr', email='test@gmail.com', password='top_secret')
        self.owner.save()
        self.item = DueItem(name="Hulu", date="03/31/2016", amount=10.00, recurrence="MO", owner=self.owner)
        self.item.save()

    def tearDown(self):
        self.item.delete()
        self.owner.delete()

    def test_can_see_home_pages(self):
        self.assertEqual(self.client.get('/').status_code, 200)

    def test_cant_see_dashboard(self):
        response = self.client.get('/dashboard/', follow=True)
        self.assertRedirects(response, '/', status_code=302, target_status_code=200)

    def test_cant_create_item(self):
        item_data = {'name':"Hulu", 'date':"", 'amount':10.00, 'recurrence':"MO"}
        response = self.client.post('/due_item/', item_data, follow=True)
        self.assertRedirects(response, '/', status_code=302, target_status_code=200)

    def test_cant_update_item(self):
        item_data = {'name':"Hulu", 'date':"", 'amount':10.00, 'recurrence':"MO"}
        response = self.client.post('/due_item/update/'+str(self.item.pk)+'/', item_data, follow=True)
        self.assertRedirects(response, '/', status_code=302, target_status_code=200)

    def test_cant_delete_item(self):
        response = self.client.get('/due_item/delete/'+str(self.item.pk)+'/', follow=True)
        self.assertRedirects(response, '/', status_code=302, target_status_code=200)

    def test_can_register(self):
        reg_form = {"username":'test-usr2', "email":'test2@gmail.com', "password1":'top_secret2', "password2":'top_secret2'}
        response = self.client.post('/register/', reg_form, follow=True)
        self.assertRedirects(response, '/', status_code=302, target_status_code=200)
        self.assertContains(response, 'We sent you and email. Please confirm your address.')

        new_users = User.objects.filter(username='test-usr2').all()
        self.assertEqual(len(new_users), 1)
        profiles = UserProfile.objects.filter(user=new_users[0]).all()
        self.assertEqual(len(profiles), 1)
        response = self.client.get('/confirm/'+profiles[0].hash, follow=True)
        self.assertContains(response, 'Account Activated.')

        user = User.objects.get(pk=new_users[0].pk)
        self.assertEqual(user.is_active, True)

        profiles.delete()
        new_users.delete()

    def test_inactive_cant_login(self):
        user = User.objects.create_user(username='test-usr2', email='test2@gmail.com', password='top_secret2')
        user.is_active = False
        user.save()

        response = self.client.post('/login/',{'username':user.username, 'password':'top_secret2'}, follow=True)
        self.assertEqual(response.context['request'].user.is_authenticated(), False)

from dashboard.models import *
from dashboard.views import *
from django.test import TestCase #, RequestFactory
from django.test.client import Client
from django.contrib import auth
from django.contrib.auth.models import AnonymousUser, User
from django.core.urlresolvers import resolve
import requests

class AnoymousTestCase(TestCase):

    def setUp(self):
        self.client = Client()

        self.owner = User.objects.create_user(username='test-usr', email='test@gmail.com', password='top_secret')
        self.owner.save()
        self.item = DueItem(name="Hulu", date="03/31/2016", amount=10.00, recurrence="MO", owner=self.owner)
        self.item.save()

        self.client.login(username=self.owner.username, password='top_secret')

    def tearDown(self):
        self.client.logout()
        self.item.delete()
        self.owner.delete()

    def test_can_see_dashboard(self):
        response = self.client.get('/dashboard/')
        self.assertEqual(response.status_code, 200) 

    def test_can_create_item(self):
        item_data = {'name':"Hulu", 'date':"", 'amount':10.00, 'recurrence':"MO"}
        response = self.client.post('/due_item/', item_data)
        self.assertEqual(response.status_code, 200)

    def test_can_update_item(self):
        item_data = {'name':"Hulu2", 'date':"03/30/2016", 'amount':11.00, 'recurrence':"YE"}
        response = self.client.post('/due_item/update/'+str(self.item.pk)+'/', item_data)
        self.assertEqual(response.status_code, 200)

        item = DueItem.objects.get(pk=self.item.pk)
        self.assertEqual(item.name, "Hulu2")
        self.assertEqual(item.date, "03/30/2016")
        self.assertEqual(item.amount, 11.00)
        self.assertEqual(item.recurrence, "YE")

        self.item = DueItem(name="Hulu", date="03/31/2016", amount=10.00, recurrence="MO", owner=self.owner)
        self.item.pk = item.pk
        self.item.save()

    def test_can_delete_item(self):
        response = self.client.get('/due_item/delete/'+str(self.item.pk)+'/')
        self.assertEqual(response.status_code, 200)

        self.item = DueItem(name="Hulu Bill", date="03/31/2016", amount=10.00, recurrence="MO", owner=self.owner)
        self.item.save()
   
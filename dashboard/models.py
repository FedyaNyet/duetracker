from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from binascii import hexlify

import os

class DueItem(models.Model):

    DAILY = "DA"
    MONTHLY = 'MO'
    ANNUALLY = 'YE'
    CUSTOM = 'CU'
    RECURRENCES = (
        (DAILY, 'Daily'),
        (MONTHLY, 'Monthly'),
        (ANNUALLY, 'Annually'),
        (CUSTOM, 'Specific Dates'),
    )

    name = models.CharField(max_length=30)
    date = models.CharField(max_length=2000)
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    recurrence = models.CharField(max_length=2, choices=RECURRENCES, default=MONTHLY)
    owner = models.ForeignKey(User, default=1)


class UserProfile(models.Model):
    
    def _createHash():
        """This function generate 10 character long hash"""
        return hexlify(os.urandom(15))

    hash = models.CharField(max_length=30, unique=True, default=_createHash)
    user = models.OneToOneField(User)
# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-22 20:03
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dashboard', '0004_auto_20160303_1900'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hash', models.CharField(max_length=30, unique=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='dueitem',
            name='recurrence',
            field=models.CharField(choices=[('DA', 'Daily'), ('MO', 'Monthly'), ('YE', 'Annually'), ('CU', 'Specific Dates')], default='MO', max_length=2),
        ),
    ]

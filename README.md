# [DueTracker](http://www.duetracker.com/)
An application that lets you track your subscriptions and payment due dates. 

## Getting Started
Here you will learn how to configure you computer to run this application.
    
### Pre-Requisites
You will first require some prerequisites to get started, namely:    
1. [PostgreSQL](http://www.postgresql.org/) (You can use another provider, but it will require an additional step.)    
2. [VirtualEnv](https://virtualenv.pypa.io/en/latest/)    
3. [Python3.5.1](https://pypi.python.org/pypi/piplapis-python3/5.1)     
Once you have all these installed on your machine, proceed to setup
     
### Setup    
start with navigating the the project directory, start a virtual virtual environment, activate it then install all the package dependencies:     
```
$ cd <project-dir>
$ virtualenv venv
$ source venv/bin/activate
(venv)$ pip install -r requirements.txt
```
_If you decided to use a non-PostgreSQL database, make sure to install the proper python adapter. For example: `(venv)$ pip install mysql-connector-python`._        
    
Next you need to configure the project's environment variables. Create a `.env` file in the root of the project to store said variables. It should contain the following keys, replacing items in `<  >` accordingly:
```
HTTP_HOST="127.0.0.1:8000/"
SECRET_KEY="<a-random-string-of-length-50>"
DEBUG=True
DATABASE_URL="<database-connection-string>" #see https://github.com/kennethreitz/dj-database-url#url-schema

EMAIL_HOST="<test-email-host>"
EMAIL_HOST_USER="<test-email-user>"
EMAIL_HOST_PASSWORD="<test-email-password>"
EMAIL_PORT=<test-email-port>
EMAIL_USE_TLS=<True or False>
```
Finally, you need to make sure your project contains the preper schema.
```
(venv)$ python manage.py migrate
```
Now run the application!
```
(venv)$ python manage.py runserver
```
